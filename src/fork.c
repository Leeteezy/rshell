#include <unistd.h>
#include <sys/types.h>

int _fork() {
    pid_t pid = fork();
    return pid;
}

