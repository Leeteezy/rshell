extern crate libc;

extern {
    fn _fork() -> libc::c_int;
    fn _get_pid() -> libc::c_int;
}

fn main() {
    unsafe {
        let pid = _fork();
        match pid {
            0 => println!("I am the parent process"),
            _ => println!("I am the child process"),
        }
    }
}
