extern crate gcc;

fn main() {
    gcc::Config::new().file("src/get_pid.c").compile("libgetpid.a");
    gcc::Config::new().file("src/fork.c").compile("libfork.a");
}
