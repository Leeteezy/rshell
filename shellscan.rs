use std::io;
use std::fmt;

#[allow(dead_code)]
#[derive(Debug)]
enum TokenKind {
    Pipe,
    Export,
    LeftArrow,
    RightArrow,
    Argument,
    EnvVariable,
    SemiColon,
    LeftParen,
    RightParen,
    LeftBracket,
    RightBracket,
    Ampersand,
    Number,
    Not,
}

impl fmt::Display for TokenKind {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

#[derive(PartialEq, Eq, Clone, Copy)]
#[allow(dead_code)]
enum State {
    Start,
    Whitespace,
    Pipe,
    Comment,
    Ampersand,
    EnvVariable,
    Id,
    LeftBracket,
    RightBracket,
    LeftBrace,
    RightBrace,
    LeftParen,
    RightParen,
    LeftArrow,
    RightArrow,
    SemiColon,
    Not,
    OpenQuote,
    Backslash,
    CloseQuote,
}

#[allow(dead_code)]
struct Token {
    kind: TokenKind,
    lexeme: String,
}

impl fmt::Display for Token {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} {}", self.kind.to_string(), self.lexeme)
    }
}

#[allow(dead_code)]
struct Transition {
    start: State,
    character: char,
    next: State,
}

impl Transition {
    fn found_transition(&self, c: char, state: State) -> bool {
        c == self.character && state == self.start
    }
}

// can be used to define keywords
fn create_token(state: State, characters: &Vec<char>) -> Token {
    let s: String = characters.iter().cloned().collect();
    match state {
        State::Id => {
            match s.as_ref() {
                "export" => Token { kind: TokenKind::Export, lexeme: String::from("export") },
                _ => Token { kind: TokenKind::Argument, lexeme: s },
            }
        },
        _ => Token { kind: TokenKind::Argument, lexeme: s },
    }
}

fn define_transitions() -> Vec<Transition> {
    let mut transitions: Vec<Transition> = Vec::new();
    let whitespace = String::from(" \t\n\r");
    let alphabet = String::from(".*_-abcdefghijklmnopqrstuvwxxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789");
    let reserved = String::from("$;|#&{}()[]<>!\"");
    for c in alphabet.chars() {
        transitions.push(Transition { start: State::Start, character: c, next: State::Id });
        transitions.push(Transition { start: State::Backslash, character: c, next: State::OpenQuote });
        transitions.push(Transition { start: State::Id, character: c, next: State::Id });
        transitions.push(Transition { start: State::EnvVariable, character: c, next: State::EnvVariable });
        transitions.push(Transition { start: State::OpenQuote, character: c, next: State::OpenQuote });
    }
    for c in reserved.chars() {
        transitions.push(Transition { start: State::Backslash, character: c, next: State::OpenQuote });
    }
    for c in whitespace.chars() {
        transitions.push(Transition { start: State::Start, character: c, next: State::Whitespace });
    }
    transitions.push(Transition { start: State::Start, character: '$', next: State::EnvVariable });
    transitions.push(Transition { start: State::Start, character: ';', next: State::SemiColon });
    transitions.push(Transition { start: State::Start, character: '|', next: State::Pipe });
    transitions.push(Transition { start: State::Start, character: '#', next: State::Comment });
    transitions.push(Transition { start: State::Start, character: '&', next: State::Ampersand });
    transitions.push(Transition { start: State::Start, character: '(', next: State::LeftParen });
    transitions.push(Transition { start: State::Start, character: ')', next: State::RightParen });
    transitions.push(Transition { start: State::Start, character: '[', next: State::LeftBracket });
    transitions.push(Transition { start: State::Start, character: ']', next: State::RightBracket });
    transitions.push(Transition { start: State::Start, character: '{', next: State::LeftBrace });
    transitions.push(Transition { start: State::Start, character: '}', next: State::RightBrace });
    transitions.push(Transition { start: State::Start, character: '<', next: State::LeftArrow });
    transitions.push(Transition { start: State::Start, character: '>', next: State::RightArrow });
    transitions.push(Transition { start: State::Start, character: '!', next: State::Not });
    transitions.push(Transition { start: State::Start, character: '"', next: State::OpenQuote });
    transitions.push(Transition { start: State::OpenQuote, character: '"', next: State::CloseQuote });
    transitions.push(Transition { start: State::OpenQuote, character: '\\', next: State::Backslash });
    transitions
}

fn define_final_states() -> Vec<State> {
    let mut final_states: Vec<State> = Vec::new();
    final_states.push(State::EnvVariable);
    final_states.push(State::Id);
    final_states.push(State::SemiColon);
    final_states.push(State::Pipe);
    final_states.push(State::Comment);
    final_states.push(State::Whitespace);
    final_states.push(State::CloseQuote);
    final_states.push(State::LeftParen);
    final_states.push(State::RightParen);
    final_states.push(State::LeftArrow);
    final_states.push(State::RightArrow);
    final_states.push(State::LeftBrace);
    final_states.push(State::RightBrace);
    final_states.push(State::RightBracket);
    final_states.push(State::LeftBracket);
    final_states.push(State::Ampersand);
    final_states.push(State::Not);
    final_states
}

fn scan_line(line: &String, transitions: &Vec<Transition>, final_states: &Vec<State>) -> Vec<Token> {
    let mut whitespace_separated = true;
    let charstr = line.chars();
    let mut tokens: Vec<Token> = Vec::new();
    let mut cur_state = State::Start;
    let mut acc: Vec<char> = Vec::new();
    let mut found_transition = false;
    for (i, character) in charstr.enumerate() {
        if cur_state == State::Comment {
            break;
        }
        for transition in transitions.iter() {
            if transition.found_transition(character, cur_state) {
                cur_state = transition.next;
                acc.push(character);
                found_transition = true;
                break;
            }
        }
        if !found_transition && final_states.contains(&cur_state) && cur_state == State::Whitespace {
            cur_state = State::Start;
            whitespace_separated = true;
        } else if !found_transition && final_states.contains(&cur_state) {
            tokens.push(create_token(cur_state, &acc));
            acc.clear();
            cur_state = State::Start;
            whitespace_separated = false;
        } else if !found_transition {
            panic!("ERROR at character {}", i);
        }
        found_transition = false;
    }
    if final_states.contains(&cur_state) && !(cur_state == State::Comment || cur_state == State::Whitespace) {
        tokens.push(create_token(cur_state, &acc));
    }
    if !final_states.contains(&cur_state) {
        panic!("Unexpected end of string");
    }
    tokens
}

fn output_tokens(tokens: &Vec<Token>) {
    for token in tokens.iter() {
        println!("{}", token);
    }
}

fn main() {
    println!("Rust scanner, Leeteezy 2017");

    let transitions = define_transitions();
    let final_states = define_final_states();
    let mut line = String::new();
    io::stdin().read_line(&mut line)
        .expect("Failed to read line");
    let tokens = scan_line(&line, &transitions, &final_states);
    output_tokens(&tokens);
}

